.. Python Simple DI documentation master file, created by
   sphinx-quickstart on Tue Feb 11 11:00:04 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python Simple DI's documentation!
============================================

.. toctree::
   :maxdepth: 2
   
   installation
   tutorial

.. automodule:: di
    :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

